# Fuhur

Fuhur meant to be a small deployment server for _one machine environment_ with the capability of extending it to multiple machines.

The base idea is that the **programs** you want **to run on your server** (for example a static website or a simple nodejs application) **are built by an external service** (like gitlab ci/cd) but you still need to **deliver** those **programs to your own server**.

That's where _Fuhur_ can help. It has two parts, one is the **API server**, the other is the **agent**.

The webserver must be called when you want to deliver some software. That call will create a job what will be processed by the agent.

# API Server

This package is the **Agent** part of _Fuhur_.

## Environment variables

### `FUHUR_CONFIG_FILE`

`String`

Configuration in JSON format.

Example:

`FUHUR_CONFIG_FILE=/etc/fuhur.json`

### `FUHUR_CONFIG_DIR`

`String`

Directory where configuration files are stored:

Example:

`FUHUR_CONFIG_DIR=/etc/fuhur.d`

### `FUHUR_SCRIPTS_DIR`

`String`

Directory where fuhur job scripts are stored.

Example:

`FUHUR_SCRIPTS_DIR=/usr/local/share/fuhur`

### `FUHUR_SIGN_SECRET`

`String`

Secret key which is shared with Fuhur API server. When Fuhur API server creates a job it signed with a preconfigured key, that ensures that the job is coming from a known source.

Example:

`FUHUR_SIGN_SECRET=abcdefghijklmnopqrstuvwxyz1234567890`

### `FUHUR_REDIS_ADDR`

`String`

Address of the redis server

Default: `localhost:6379`

Example:

`FUHUR_REDIS_ADDR=localhost:6379`

### `FUHUR_REDIS_PASSWORD`

`String`

Password for the redis server

Example:

`FUHUR_REDIS_PASSWORD=abcdefghijklmnopqrstuvwxyz1234567890`

### `FUHUR_REDIS_DB`

`Int`

The number of the database on the Redis server where Fuhur can act.

Default: `1`

Example:

`FUHUR_REDIS_DB=10`

## Configuration format

Usually to deploy an application you will need to execute several commands.

For example:

 - Download new application version
 - Stop running application
 - Replace old application with the new
 - Start new application
 - Send notification that new application is deployed

Usually you can do them manually, line by line or create a scripts for them. But instead of SSH-ing into your server and run these commands Fuhur Agent can execute them without any interaction.

You should define in the configuration file what command should be executed as part of a job.
The configuration is in JSON format, each task is an entry in an array and they will be called after each other.

### Job

### Command

### Args

```json
[
{
	"job": "deploy application",
	"command": "/usr/bin/wget"
	"args":[
		"/usr/bin/wget",
		"-qO",
		"/tmp/latest.bin",
		"https://build.example.com/latest.bin"
	]
}
]
```

The code above will tell to Fuhur Agent to use `/usr/bin/wget` to download `latest.bin` from `https://build.example.com/latest.bin` and place it into `/tmp/latest.bin`.

You can use simple shell commands or you can create any kind of shell script, or other executable program to do the tasks.

### Dir

There is a `FUHUR_SCRIPTS_DIR` which is important for this. Fuhur Agent will search for executables inside this dreictory. In addition it's a good practice to create a directory inside `FUHUR_SCRIPTS_DIR` for each job where all of the executables will be stored.

So lets assume we created a `myapp` directory inside `FUHUR_SCRIPTS_DIR` and we added `download.sh`, `stop_service.sh`, `start_service.sh`, `install.sh` and `notify.sh` inside that directory.

Then you can create the following config to run the tasks for `myapp` job:

```json
[
{
	"job": "myapp",
	"command": "download.sh",
	"dir": "myapp"
},
{
	"job": "myapp",
	"command": "stop_service.sh",
	"dir": "myapp"
},
{
	"job": "myapp",
	"command": "install.sh",
	"dir": "myapp"
},
{
	"job": "myapp",
	"command": "start_service.sh",
	"dir": "myapp"
},
{
	"job": "myapp",
	"command": "notify.sh",
	"dir": "myapp"
}
]
```

Each of the scripts will be called after the pervious finished successfully (exited with `0`).

### Environment variables

You can also pass environment variables to a script:

```json
{
	"job": "myapp",
	"command": "download.sh",
	"dir": "myapp"
	"env": ["APPLICATION_VERSION=v1.0.1"]
}
```

The `APPLICATION_VERSION` environment variable will set for `download.sh` and it's value will be `v1.0.1`, the script then can decide what do do with it.
