package main

import (
	"fmt"
	"os"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/fuhur/execconf"
)

func loadExecConfigs(job string) ([]execconf.ExecConf, error) {
	var execConfigs []execconf.ExecConf

	if _, err := os.Stat(config.ConfigFile); !os.IsNotExist(err) {
		fileExecConfigs, err := execconf.ReadExecConfFile(config.ConfigFile)
		if err != nil {
			log.WithFields(log.Fields{
				"configFilePath": config.ConfigFile,
			}).Error(err)
		}

		execConfigs = append(execConfigs, fileExecConfigs...)
	}

	info, err := os.Stat(config.ConfigDir)

	if err != nil {
		return nil, err
	}

	if info.IsDir() {
		dirConfigs, err := execconf.ReadExecConfDir(config.ConfigDir)
		if err != nil {
			log.WithFields(log.Fields{
				"configDirPath": config.ConfigDir,
			}).Error(err)
		}

		execConfigs = append(execConfigs, dirConfigs...)
	}

	var output []execconf.ExecConf

	for _, execConf := range execConfigs {
		if execConf.Job == job {
			output = append(output, execConf)
		}
	}

	return output, nil
}

func extendExecConfig(execConfig execconf.ExecConf, env map[string]string) execconf.ExecConf {
	for name, value := range env {
		execConfig.Env = append(execConfig.Env, fmt.Sprintf("%s=%s", name, value))
	}

	return execConfig
}

func extendExecConfigs(env map[string]string, execConfigs []execconf.ExecConf) ([]execconf.ExecConf, error) {
	for i, execConfig := range execConfigs {
		execConfigs[i] = extendExecConfig(execConfig, env)
	}

	return execConfigs, nil
}

func hasConfigs(execConfigs []execconf.ExecConf) bool {
	return len(execConfigs) > 0
}
