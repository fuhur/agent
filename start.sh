#!/bin/sh
export PATH=/usr/local/bin/:/usr/bin:/usr/local/bin
export FUHUR_CONFIGFILE=./configuration.example.json
export FUHUR_CONFIGDIR=./configuration.example.d/
export FUHUR_SCRIPTSDIR=./scripts
export FUHUR_SIGNSECRET=a
./agent
