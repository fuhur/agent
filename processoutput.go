package main

import (
	"bufio"
	"io"
)

// ProcessOutput a struct to store process std out and std err
type ProcessOutput struct {
	Stdout io.ReadCloser
	Stderr io.ReadCloser
}

func scanIO(io io.Reader, c chan []byte, done chan int) {
	scanner := bufio.NewScanner(io)
	for scanner.Scan() {
		c <- scanner.Bytes()
	}

	done <- 1
}

func writeProcessOutput(outputs *ProcessOutput, loggers execLoggers) {
	info := loggers.Info
	err := loggers.Error

	outputChan := make(chan []byte)
	errorChan := make(chan []byte)
	q := make(chan int)

	go scanIO(outputs.Stdout, outputChan, q)
	go scanIO(outputs.Stderr, errorChan, q)

	quitCount := 0
	for {
		select {
		case errBytes := <-errorChan:
			err.Write(errBytes)
		case outBytes := <-outputChan:
			info.Write(outBytes)
		case <-q:
			quitCount++
		}

		if quitCount >= 2 {
			break
		}
	}
}
