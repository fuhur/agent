package main

import (
	"io"

	log "github.com/Sirupsen/logrus"
)

// LogrusLogger to define logrus properties
type LogrusLogger struct {
	Fields   log.Fields
	LogLevel log.Level
	Logger   *log.Logger
}

func (l LogrusLogger) Write(p []byte) (n int, err error) {
	entry := l.Logger.WithFields(l.Fields)

	switch l.LogLevel {
	case log.ErrorLevel:
		entry.Error(string(p))
		return len(p), nil
	case log.InfoLevel:
		entry.Info(string(p))
		return len(p), nil
	default:
		return 0, nil
	}
}

// Logger Struct which implements io.Wirter interface
// to write into other writers simultaneously
// It holds a list of io.Writer where then it will write
type Logger struct {
	Loggers []io.Writer
}

func (l Logger) Write(p []byte) (n int, err error) {
	for _, l := range l.Loggers {
		_, err := l.Write(p)

		if err != nil {
			return 0, err
		}
	}

	return len(p), nil
}

type execLoggers struct {
	Info  Logger
	Error Logger
}

func getExecLoggers(jobName string, stdLogger *log.Logger) execLoggers {
	errorLogger := Logger{
		Loggers: []io.Writer{
			LogrusLogger{
				Fields: log.Fields{
					"job": jobName,
				},
				LogLevel: log.ErrorLevel,
				Logger:   stdLogger,
			},
		},
	}

	infoLogger := Logger{
		Loggers: []io.Writer{
			LogrusLogger{
				Fields: log.Fields{
					"job": jobName,
				},
				LogLevel: log.InfoLevel,
				Logger:   stdLogger,
			},
		},
	}

	return execLoggers{infoLogger, errorLogger}
}
