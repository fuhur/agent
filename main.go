package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"os/signal"

	log "github.com/Sirupsen/logrus"
	"github.com/coreos/go-systemd/daemon"
	"github.com/go-redis/redis"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/fuhur/execjob"
)

var config Config
var redisClient *redis.Client

const redisKeyPrefix string = "redis_logs:"
const jobChannelName = "job"

var version string

var versionFlag bool

func subscribe(done chan bool) *redis.PubSub {
	pubsub := redisClient.Subscribe(jobChannelName)

	if _, err := pubsub.Receive(); err != nil {
		log.Fatal(err)
	}

	ch := pubsub.Channel()

	go func() {
		for {
			select {
			case msg := <-ch:
				var signedJob execjob.SignedJob

				err := json.Unmarshal([]byte(msg.Payload), &signedJob)

				if err != nil {
					log.Error(err)
				} else {
					isValid, err := execjob.IsValidSignature(config.SignSecret, signedJob)

					if err != nil {
						log.Error(err)
					} else if isValid {
						job := signedJob.Job
						log.Println("Exec job", job.JobName, job.JobID)
						go execJob(job)
					} else {
						log.Warning("Invalid signature")
					}
				}
			case <-done:
				return
			}
		}
	}()

	return pubsub
}

func init() {
	flag.BoolVar(&versionFlag, "version", false, "show version number")
	flag.Parse()
	if !versionFlag {
		if err := envconfig.Process("FUHUR", &config); err != nil {
			log.Fatal(err)
		}

		redisClient = redis.NewClient(&redis.Options{
			Addr:     config.RedisAddr,
			Password: config.RedisPassword,
			DB:       config.RedisDB,
		})
	}
}

func main() {
	if versionFlag {
		fmt.Printf("%s\n", version)
		os.Exit(0)
	}
	done := make(chan bool)
	pubsub := subscribe(done)
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt)

	select {
	case <-osSignals:
		log.Info("Stop server")
		done <- true
		pubsub.Close()
		daemon.SdNotify(false, "STOPPING=1")
	}
}
