package main

import (
	"fmt"
	"strconv"

	"github.com/Ajnasz/logrus-redis"
	log "github.com/Sirupsen/logrus"
	"gitlab.com/fuhur/execjob"
)

func execJob(job execjob.Job) {
	stdLogger := log.New()
	hook := logrusredis.NewLogrusRedis(redisClient, redisKeyPrefix+job.JobID)

	stdLogger.Hooks.Add(hook)

	loggers := getExecLoggers(job.JobName, stdLogger)
	execConfigs, err := loadExecConfigs(job.JobName)
	if err != nil {
		loggers.Error.Write([]byte(err.Error()))

		return
	}

	if !hasConfigs(execConfigs) {
		loggers.Error.Write([]byte("Configuration not found"))
		return
	}
	execConfigs, err = extendExecConfigs(job.Env, execConfigs)

	if err != nil {
		loggers.Error.Write([]byte(err.Error()))
		return
	}

	for _, execConf := range execConfigs {
		loggers.Info.Write([]byte(fmt.Sprintf("execute job %s", execConf.Command)))

		jobEnd := make(chan int)

		outputs, err := runJob(execConf, jobEnd)

		if outputs == nil {
			loggers.Error.Write([]byte(err.Error()))
			break
		}

		if err != nil {
			loggers.Error.Write([]byte(err.Error()))
			loggers.Error.Write([]byte("Job exection failed"))
			return
		}

		writeProcessOutput(outputs, loggers)

		exitCode := <-jobEnd

		if exitCode != 0 {
			loggers.Error.Write([]byte("Job exited with code " + strconv.Itoa(exitCode)))
			break
		} else {
			loggers.Info.Write([]byte("Job exited with code " + strconv.Itoa(exitCode)))
		}

		loggers.Info.Write([]byte(fmt.Sprintf("execute job finished %s", execConf.Command)))
	}

	loggers.Info.Write([]byte("execute job package finished"))
	loggers.Info.Write([]byte("EOL"))
}
