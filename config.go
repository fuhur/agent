package main

// Config is a struct to define configuration
type Config struct {
	ConfigFile    string `required:"true"`
	ConfigDir     string `required:"true"`
	ScriptsDir    string `required:"true"`
	SignSecret    string `required:"true"`
	RedisAddr     string `default:"localhost:6379"`
	RedisPassword string
	RedisDB       int `default:"1"`
}
